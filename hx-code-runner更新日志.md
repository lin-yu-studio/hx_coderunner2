# hx-code-runner更新日志

--------

### 0.1.1（2023-09-01）

- 修复WIndows上运行C/C++的问题;
- 修改typescript的运行方式为ts-node;
- 插件运行环境适配到MacOS、Linux和Windows
- 存在issue：openAndRunTerminal()的函数问题待修复