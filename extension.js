// @Time    :   Created in 2023-08-30 20:29:05
// @Author  :   Fox
// @Contact :   Fox1949@126.com
// @License :   (C)Copyright 2022-2023, LinYuStudio-NLPR-CASIA
// @Editor  :   HbuilderX
// @Version :   1.0.0
// @Desc    :   hx code runner


const hx = require("hbuilderx");
const {
    openRunnerMap
} = require("./src/utils.js");
const main = require("./src/main.js");
const {
    checkUpgrade,
    about
} = require("./src/about.js");

/**
 * @description 插件激活函数
 */
function activate(context) {

    // 检查更新
    checkUpgrade();

    // 运行代码片段
    let disposable = hx.commands.registerCommand("hxCodeRunner.run", (param) => {
        main(param);
    });

    context.subscriptions.push(disposable);

    // languages map
    let languagesExecutorMap = hx.commands.registerCommand('hxCodeRunner.languagesRunnerMap', () => {
        openRunnerMap();
    });
    context.subscriptions.push(languagesExecutorMap);

    // 关于
    let aboutPlugins = hx.commands.registerCommand('hxCodeRunner.about', () => {
        about();
    });
    context.subscriptions.push(aboutPlugins);
}

let deactivate = () => {

};

module.exports = {
    activate,
    deactivate
};