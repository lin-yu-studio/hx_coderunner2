// @Time    :   Created in 2023-08-30 21:46:39
// @Author  :   Fox
// @Contact :   Fox1949@126.com
// @License :   (C)Copyright 2022-2023, LinYuStudio-NLPR-CASIA
// @Editor  :   HbuilderX
// @Version :   1.0.0
// @Desc    :   主入口文件


const hx = require("hbuilderx");
const path = require("path");
const fs = require("fs");
const {
    exec
} = require('child_process');
const os = require("os");

const language = require('./languages.js');
const help = require('./helper.js');
const utils = require('./utils.js');


// 获取操作平台 win32 
const osName = os.platform();

// 特殊的语言，即在操作系统上无需安装任何环境都可以运行的脚本
let specialLanguageList = ["sh", "bat", "out"];

class Code {

    constructor(projectPath, filePath) {
        this.projectPath = projectPath;
        this.filePath = filePath;
    }

    /**
     * @description 检查命令是否存在
     */
    checkProgram(commands) {
        let cmd = osName == 'darwin' ? `which ${commands}` : `where ${commands}`;
        return new Promise((resolve, reject) => {
            exec(cmd, function(error, stdout, stderr) {
                if (error) {
                    reject(error)
                };
                resolve(true);
            });
        })
    };

    /**
     * @description 获取用户自定义的配置
     * @param {String} fileSuffix 文件后缀
     */
    getUserCustomConfig(fileSuffix) {
        // 插件配置目录文件
        let userConfigFile = utils.getConfigDir();
        if (userConfigFile == false) {
            return false;
        }
        try {
            let tmp = path.join(hx.env.appData, 'hx-code-runner', 'RunnerMap.js');
            let userSet = require(tmp);
            let runProgramPath = userSet[fileSuffix];
            if (runProgramPath == undefined || runProgramPath == '') {
                return false;
            }
            let stat = fs.statSync(runProgramPath);
            if (stat.isFile()) {
                return runProgramPath;
            }
            return false;
        } catch (e) {
            console.log(e);
            return false;
        }
    };

    async run() {
        let filename = (this.filePath).replace(path.join(this.projectPath, '/'), '');

        // 文件后缀
        let ext = path.extname(this.filePath);
        // 文件名
        let fileNameWithoutExt = path.parse(filename).name;
        ext = ext.toLowerCase().substring(1);
        let program = language[ext];
        // 组织运行命令
        let cmd = "";

        // 判断是否是C或者C++或者增加其它的
        if (ext === 'c' || ext === 'cpp') {
            // 判断系统是否为darwin
            if (osName == "darwin" || osName == "linux") {
                cmd = `${program} ${filename} -o ${fileNameWithoutExt}.out && ./${fileNameWithoutExt}.out`;
            } else if (osName == "win32") {
                cmd = `${program} ${filename} -o ${fileNameWithoutExt}.exe && ${fileNameWithoutExt}.exe`;
            }
        } else {
            cmd = `${program} ${filename}`;
        }

        // 特殊程序, 比如shell、bat
        if (specialLanguageList.includes(ext)) {
            cmd = osName == 'darwin' ? `./${filename}` : filename;
        } else {
            // 检查系统
            let SystemEnv = await this.checkProgram(program).catch(error => {
                return false
            });
            // 检查用户设置
            let userSet = await this.getUserCustomConfig(ext);
            if (SystemEnv == false && userSet == false) {
                let msgKey = program ? program : ext;
                try {
                    let p = program.split(' ')[0];
                    let programWebstie = help[p]["website"];
                    let programName = help[p]["name"];
                    hx.window.showErrorMessage(
                        `hx-code-runner: 当前电脑未检测到 ${msgKey} 运行环境。<br/>请打开官网安装下载相关程序。<a href="${programWebstie}">${programName}官网</a>`,
                        ["我知道了"]);
                } catch (e) {
                    hx.window.showErrorMessage(`hx-code-runner: 当前电脑未检测到 ${msgKey} 运行环境。`, ["我知道了"]);
                }
                return;
            }
            if (userSet != false) {
                cmd = `${userSet} ${filename}`;
            }
        }

        if (osName != 'darwin') {
            let drive = this.projectPath.substr(0, 1);
            cmd = `cmd /K "${drive}: && cd ${this.projectPath} && ${cmd}"`;
        } else {
            cmd = `cd ${this.projectPath} && ` + cmd;
        }

        let runParam = {
            cmd: cmd,
            rootPath: this.projectPath,
        }


        hx.window.openAndRunTerminal(runParam).then(data => {
            let {
                error
            } = data;
            if (error) {
                console.error('hx-code-runner: ' + error);
            }
        });


    }
}

/**
 * @description 主入口文件
 * @param {Object} param 项目管理器或编辑器选中的信息
 */
function main(param) {
    if (param == null) {
        hx.window.showErrorMessage("hx-code-runner: 请选中文件要执行的文件");
        return;
    };

    let filePath, projectPath;
    try {
        let {
            metaType
        } = param;
        if (metaType == 'TextEditor') {
            filePath = param.document.uri.fsPath;
            projectPath = param.document.workspaceFolder.uri.fsPath;
        };
        if (metaType == undefined) {
            filePath = param.fsPath;
            projectPath = param.workspaceFolder.uri.fsPath;
        };
    } catch (e) {
        return;
    };

    // 运行
    let rc = new Code(projectPath, filePath);
    rc.run();
}

module.exports = main;