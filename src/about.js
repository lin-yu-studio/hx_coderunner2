// @Time    :   Created in 2023-08-30 21:19:48
// @Author  :   Fox
// @Contact :   Fox1949@126.com
// @License :   (C)Copyright 2022-2023, LinYuStudio-NLPR-CASIA
// @Editor  :   HbuilderX
// @Version :   1.0.0
// @Desc    :   插件相关

const hx = require("hbuilderx");
const os = require("os");
const {
    version,
    id
} = require("../package.json");

// 操作系统版本
const osVersion = os.version();
// 操作系统
const osName = os.platform();
// 插件id
const pluginId = id;
// 插件更新提示
const pluginUpdatePrompt = `${pluginId}.updatePrompt`;
// 插件更新时间
const pluginUpdatePromptTime = `${pluginId}.updatePromptTime`;
// 判断是否弹出气泡
let isPopUpWindow = false;


function isJSON(str) {
    if (typeof str === "string") {
        try {
            // 解析json对象
            let obj = JSON.parse(str);
            if (typeof obj === "object" && obj) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }
}

/**
 * @description 更新弹窗，点击【以后再说】，则本周内不再自动弹窗提示
 */
function showUpgradeBox(localVersion, marketPluginVersion) {
    if (marketPluginVersion == '' || marketPluginVersion == undefined) {
        return;
    };
    let lastChar = marketPluginVersion.charAt(marketPluginVersion.length - 1);
    let versionDescription = `【${pluginId}】发布 ${marketPluginVersion} 版本！`;
    // 弹窗信息
    let msg = versionDescription +
        `当前 ${localVersion} 版本。` +
        `<a href="https://ext.dcloud.net.cn/plugin?name=${pluginId}">更新日志</a>` +
        '<br/><br/>注意：更新后，重启HBuilderX才能生效。';

    // 弹窗的button的内容
    let btn = ['去插件市场更新', '以后再说'];

    hx.window.showInformationMessage(msg, btn).then((result) => {
        if (result === '去插件市场更新') {
            hx.env.openExternal(`https://ext.dcloud.net.cn/plugin?name=${pluginId}`);
        } else {
            let timeStamp = Math.round(new Date() / 1000) + 604800;
            let config = hx.workspace.getConfiguration();
            config.update(pluginUpdatePrompt, false).then(() => {
                config.update(pluginUpdatePromptTime, `${timeStamp}`);
            });
        }
    });
    isPopUpWindow = true;
}

/**
 * @description 关闭更新
 */
function noUpgrade(version) {
    let msg = `${pluginId}: 当前版本为 ${version}，是最新版本。`;
    let btns = ['关闭']

    let config = hx.workspace.getConfiguration();
    let updatePrompt = config.get(pluginUpdatePrompt);
    let updatePromptTime = config.get(pluginUpdatePromptTime);
    if (updatePromptTime != undefined || updatePrompt != undefined) {
        btns = ['有更新时提醒我', '关闭'];
    };

    hx.window.showInformationMessage(msg, btns).then((result) => {
        if (result === '有更新时提醒我') {
            config.update(pluginUpdatePrompt, true).then(() => {
                config.update(pluginUpdatePromptTime, '1577808001');
            });
        };
    });
};

/**
 * @description 获取线上版本号
 * @return {String} pluginVersion
 */
function getServerVersion() {
    let http = require('http');
    const versionUrl = 'http://update.dcloud.net.cn/hbuilderx/alpha/win32/plugins/index.json';
    return new Promise(function(resolve, reject) {
        http.get(versionUrl, (res) => {
            let data = "";
            res.on("data", (chunk) => {
                data += chunk;
            });
            res.on("end", () => {
                try {
                    let myPluginsVersion;
                    if (isJSON(data)) {
                        let allPlugins = JSON.parse(data);
                        let {
                            plugins
                        } = allPlugins;
                        for (let s of plugins) {
                            if (s.name == pluginId) {
                                myPluginsVersion = s.version;
                                break;
                            }
                        }
                    }
                    resolve(myPluginsVersion);
                } catch (e) {
                    reject('error');
                }
            });
            res.on("error", (e) => {
                reject('error');
                isPopUpWindow = true;
            });
        });
    })
}

/**
 * @description 自动检查更新
 */
async function checkUpgrade() {
    if (isPopUpWindow) {
        return;
    };

    // get week
    let currentTimeStamp = Math.round(new Date() / 1000);
    let config = await hx.workspace.getConfiguration();
    let updatePrompt = config.get(pluginUpdatePrompt);
    let updatePromptTime = config.get(pluginUpdatePromptTime);
    if (updatePromptTime) {
        try {
            if (updatePromptTime > currentTimeStamp) {
                return;
            }
        } catch (e) {

        };
    };
    let serverVersion = await getServerVersion();
    if (serverVersion != version) {
        showUpgradeBox(version, serverVersion);
    };
}

/**
 * @description 关于
 */
async function about() {
    let serverVersion = await getServerVersion();
    if ((serverVersion != version) && serverVersion != undefined) {
        showUpgradeBox(version, serverVersion);
    } else {
        noUpgrade(version);
    };
};

// 模块导出
module.exports = {
    checkUpgrade,
    about
};