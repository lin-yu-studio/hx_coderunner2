// @Time    :   Created in 2023-08-30 20:47:35
// @Author  :   Fox
// @Contact :   Fox1949@126.com
// @License :   (C)Copyright 2022-2023, LinYuStudio-NLPR-CASIA
// @Editor  :   HbuilderX
// @Version :   1.0.0
// @Desc    :   NaN


const hx = require("hbuilderx");
const path = require("path");
const fs = require("fs");


/**
 * @description 获取hxCodeRunner的配置文件目录
 */
async function getConfigDir() {
    // hbuilderx 应用程序 数据存放路径
    let appDataDir = hx.env.appData;
    // 在应用程序数据存放路径下找到 hx-code-runner的插件路径
    let configDir = path.join(appDataDir, 'hx-code-runner');
    // 检测文件夹是否存在
    let dirStatus = fs.existsSync(configDir);
    // 如果文件夹不存在则返回 false
    if (!dirStatus) {
        return false;
    }
    // 获取配置文件的路径
    let configFile = path.join(appDataDir, 'hx-code-runner', 'RunnerMap.js');
    // 检查配置文件是否存在
    let fileStatus = fs.existsSync(configFile);
    // 如果配置文件不存在则返回false
    if (!fileStatus) {
        return false;
    }
    // 如果存在则返回配置文件的路径
    return configFile;
}

/**
 * @description 复制文件
 * @param {Object} templatePath
 * @param {Object} targetPath
 * @param {Object} isOpenFile
 */
function copyFile(templatePath, targetPath, isOpenFile) {
    // 复制模版文件到目标文件路径
    fs.copyFile(templatePath, targetPath, (err) => {
        if (err) {
            // 如果复制不成功则弹出提示
            hx.window.showErrorMessage(targetPath + "创建失败.");
        } else {
            // 检查是否打开
            if (isOpenFile) {
                // 打开复制后的目标路径的文件
                hx.workspace.openTextDocument(targetPath);
            }
        }
    });
}

/**
 * @description 打开配置文件 方便用户自己设定各种编程语言的运行环境
 */
function openRunnerMap() {
    // hbuilderx 应用程序 数据存放路径
    let appDataDir = hx.env.appData;
    // 在应用程序数据存放路径下找到 hx-code-runner的插件路径
    let configDir = path.join(appDataDir, 'hx-code-runner');
    // 检测文件夹是否存在
    let dirStatus = fs.existsSync(configDir);
    // 如果文件夹不存在则 创建配置文件夹
    if (!dirStatus) {
        fs.mkdirSync(configDir);
    }
    // 获取配置文件的路径
    let configFile = path.join(appDataDir, 'hx-code-runner', 'RunnerMap.js');
    // 检查配置文件是否存在
    let fileStatus = fs.existsSync(configFile);
    // 如果配置文件不存在
    if (!fileStatus) {
        // 模版文件路径
        let templatePath = path.join(__dirname, "template", "RunnerMap.js");
        // 要复制到的目标路径
        let targetPath = path.join(configDir, "RunnerMap.js");
        // 复制到目标路径
        copyFile(templatePath, targetPath, true);
    } else {
        // 否则打开配置文件
        hx.workspace.openTextDocument(configFile);
    }
}


// 模块导出
module.exports = {
    getConfigDir,
    openRunnerMap
};