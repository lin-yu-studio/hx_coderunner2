// @Time    :   Created in 2023-08-30 20:45:32
// @Author  :   Fox
// @Contact :   Fox1949@126.com
// @License :   (C)Copyright 2022-2023, LinYuStudio-NLPR-CASIA
// @Editor  :   HbuilderX
// @Version :   1.0.0
// @Desc    :   编程语言 后缀及其运行命令


let languages = {
    "bat": "cmd /c",
    "c": "gcc",
    "cpp": "g++",
    "coffee": "coffee",
    "dart": "dart",
    "py": "python",
    "pyc": "python",
    "php": "php",
    "pl": "perl",
    "perl": "perl",
    "perl6": "perl6",
    "java": "java",
    "js": "node",
    "ts": "ts-node",
    "lua": "lua",
    "less": "lessc",
    "scss": "sass",
    "styl": "stylus",
    "sh": "shell",
    "swift": "swift",
    "go": "go run",
    "rb": "ruby",
    "scpt": "osascript"
};

module.exports = languages;